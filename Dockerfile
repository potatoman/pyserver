FROM python:3.8

COPY pyserver.py /pyserver.py

CMD ["python3", "/pyserver.py"]


