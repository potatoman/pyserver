from http.server import BaseHTTPRequestHandler, HTTPServer
import time

class MyHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
        self.wfile.write(b'Hello, world!')

if __name__ == '__main__':
    time.sleep(30)  # Wait for 30 seconds before starting the server
    server_address = ('', 8080)
    httpd = HTTPServer(server_address, MyHandler)
    print('Running server...')
    httpd.serve_forever()

